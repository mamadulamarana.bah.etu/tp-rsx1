package udp;
import java.io.*;
import java.net.*;

public class ReceptionUdp {

    public static void main(String[] args) {
        DatagramSocket s = null;
        if (args.length > 0) {
            try {
                s = new DatagramSocket(Integer.parseInt(args[0]));
            } catch (SocketException e) {
                System.out.println("Socket Exception");
            }
            DatagramPacket p;
            byte[] buffer = new byte[512];
            p = new DatagramPacket(buffer, buffer.length);
            try {
                s.receive(p);
            } catch (IOException e) {
                System.out.println("reception impossible");
            }

            System.out.println("paquet reçu de : " + p.getAddress() +
                    " port : " + p.getPort() +
                    " length : " + p.getLength());
            System.out.println("message : " + new String(p.getData()));

            s.close();
        }

    }

}
