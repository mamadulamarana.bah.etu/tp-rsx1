package udp;

import java.io.*;
import java.net.*;

public class MulticastEnvoieUdp {

    public static void main(String[] args) {

        MulticastSocket s = null;
        try {
            s = new MulticastSocket();

        } catch (IOException e) {
            System.out.println("Socket Exception");
            return;
        }

        String msg = args[0];
        byte[] buffer = msg.getBytes();
        InetAddress dest = null;
        try {
            dest = InetAddress.getByName("224.0.0.1");
        } catch (UnknownHostException e) {
            System.out.println("Unknown Host Adress");
            s.close();
            return;
        }
        int destPort = 7654;

        DatagramPacket p;
        p = new DatagramPacket(buffer, buffer.length, dest, destPort);
        try {
            s.send(p);
        } catch (IOException e) {
            System.out.println("envoi impossible");
        }

        s.close();

    }

}
