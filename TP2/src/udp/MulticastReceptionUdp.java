package udp;

import java.io.IOException;
import java.net.*;

public class MulticastReceptionUdp {
    public static void main(String[] args) {
        MulticastSocket s = null;

        try {
            s = new MulticastSocket(7654);
        } catch (IOException e) {
            System.out.println("Socket Exception");
            return;
        }

        InetAddress addr;
        try {
            addr = InetAddress.getByName("224.0.0.1");
        } catch (UnknownHostException e) {
            System.out.println("adresse invalide");
            s.close();
            return;
        }

        DatagramPacket p;
        byte[] buffer = new byte[512];
        p = new DatagramPacket(buffer, buffer.length);
        try {
            s.joinGroup(addr);
            s.receive(p);
        } catch (IOException e) {
            System.out.println("reception impossible");
            s.close();
            return;
        }

        System.out.println("paquet reçu de : " + p.getAddress() +
                " port : " + p.getPort() +
                " length : " + p.getLength());
        System.out.println("message : " + new String(p.getData()));

        try {
            s.leaveGroup(addr);
        } catch (IOException e) {
            s.close();
            return;
        }
        s.close();
    }

}
