package udp;
import java.io.*;
import java.net.*;

public class EmissionUdp {

    public static void main(String[] args) {

        DatagramSocket s = null;
        if (args.length > 0) {
            try {
                s = new DatagramSocket();
                
            }catch(SocketException e) {
                System.out.println("Socket Exception");
            }

            String msg = args[2];
            byte[] buffer = msg.getBytes();
            InetAddress dest = null;
            try {
                dest = InetAddress.getByName("a04p"+args[0]+".fil.univ-lille.fr");
            } catch(UnknownHostException e) {
                System.out.println("Unknown Host Adress");
            }
            int destPort = Integer.parseInt(args[1]);

            DatagramPacket p;
            p = new DatagramPacket(buffer, buffer.length, dest, destPort);
            try {
                s.send(p);
            } catch(IOException e) {
                System.out.println("envoi impossible");
            }
            
            s.close();

        }
    }

}
