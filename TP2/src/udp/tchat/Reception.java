package udp.tchat;

import java.io.IOException;
import java.net.*;

public class Reception implements Runnable {
    protected String msg;

    public Reception() {
    };

    @Override
    public void run() {
        MulticastSocket s = null;

        try {
            s = new MulticastSocket(7654);
        } catch (IOException e) {
            System.out.println("Socket Exception");
            return;
        }

        InetAddress addr;
        try {
            addr = InetAddress.getByName("224.0.0.1");
        } catch (UnknownHostException e) {
            e.printStackTrace();
            s.close();
            return;
        }
        
        try {
            s.joinGroup(addr);
        } catch (IOException e) {
            s.close();
            return;
        }

        boolean fin = false;
        while (!fin) {

            DatagramPacket p;
            byte[] buffer = new byte[512];
            p = new DatagramPacket(buffer, buffer.length);
            try {
                s.receive(p);
            } catch (IOException e) {
                System.out.println("reception impossible");
                s.close();
                return;
            }

            this.msg = new String(p.getData());

            System.out.println("paquet reçu de : " + p.getAddress() +
                    "port : " + p.getPort() +
                    "length : " + p.getLength());
            System.out.println("message reçu: " + this.msg);

            if (this.msg.toLowerCase().equals("end")) {
                try {
                    s.leaveGroup(addr);
                } catch (IOException e) {
                    s.close();
                    return;
                }
                s.close();
                fin = true;
            }
        }
        return;
    }

}
