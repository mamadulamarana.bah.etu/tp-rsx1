package udp.tchat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Scanner;

public class Envoie implements Runnable {
    protected String msg;
    protected BufferedReader msgSaisi;

    public Envoie() {
    }

    public void run() {
        MulticastSocket s = null;
        try {
            s = new MulticastSocket();

        } catch (IOException e) {
            System.out.println("Socket Exception");
            return;
        }

        InetAddress dest = null;
        int destPort = 7654;
        try {
            dest = InetAddress.getByName("224.0.0.1");
        } catch (UnknownHostException e) {
            System.out.println("Unknown Host Adress");
            s.close();
            return;
        }

        boolean fin = false;
        while (!fin) {
            this.msgSaisi = new BufferedReader(new InputStreamReader(System.in));
            try {
                this.msg = this.msgSaisi.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            byte[] buffer = this.msg.getBytes();

            DatagramPacket p;
            p = new DatagramPacket(buffer, buffer.length, dest, destPort);
            try {
                s.send(p);
            } catch (IOException e) {
                System.out.println("envoi impossible");
                s.close();
            }
            if (this.msg.toLowerCase().equals("end")) {
                s.close();
                fin = true;
            }
        }
        return;
    }

}
