package udp.tchat;

import java.util.Scanner;

public class Tchat {
    public static void main(String[] args) {

        Envoie t1 = new Envoie();
        Reception t2 = new Reception();

        Thread th1 = new Thread(t1);
        Thread th2 = new Thread(t2);

        th1.start();
        th2.start();
    }
}
