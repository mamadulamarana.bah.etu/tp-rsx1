# Rapport pour le TP2

## Exercice 1 : Fait
    package udp
### Compilation
    => EmissionUdp.java et ReceptionUdp.java
`javac -classpath src -d classes src/udp/*.java`  
`java -cp classes udp.Envoie`  
`java -cp classes udp.Reception`  

## Exercice 2 : Fait
![Multicast](screen1.png)

### Compilation
    package udp
    => MulticastEnvoie.java et MulticastReception.java 
`javac -classpath src -d classes src/udp/*.java`  
`java -cp classes udp.MulticastEnvoie`  
`java -cp classes udp.MulticastReception`  

# Q1
    => Pour émettre des paquets UDP multicast sur le réseau local,
    il nous faut :
    - Créer un socket local avec l'adresse ip du multicast du
    reseau local sur laquelle sont enregistés les machines, lier le socket
    à un numéro de port via lequel le message sera livré;
    et enfin envoyé le paquet contenant le message avec sen().

    => Pour recevoir des paquets UDP multicast, il faut :
    - Créer un socket UDP et le lier au port sur
    lequel on souhaite recevoir les paquets envoyés par
    un serveur (7654 dans notre cas), ensuite il faut rejoindre le groupe
    multicast avec joinGroup(), on pourra ensuite un paquet recevoir via la
    methode receive().

# Q2
    Pour l'envoie d'un paquet, les exceptions suivantes sont à traitées
    - L'exception IOException déclenché par un socket mal initialisé et
    un envoie qui ne s'est pas bien passé, elle se produit aussi
    si l'opération de fermeture du socket se passe mal.
    L'exception UnknownHostException qui se déclenche quand
    une adresse ip donnée n'est pas valide.

    Pour la reception d'un paquet, les exceptions suivantes :
    - L'exception IOException se déclenche si un socket est mal initialisé,
    où si une tentative de rejoindre un groupe multicast où une
    reception du paquet ne se passe pas bien, cette exception se
    déclenche aussi si la tentative de fermeture ne se passe pas
    bien. L'exception UnknownHostException qui se déclenche si
    une adresse ip multicast invalide est fourni au socket.


## Exercice 3 : Fait
![Tchat](screen2.png)

### Compilation
    package udp.tchat
    => Envoie.java, Reception.java et Tchat.java
`javac -classpath src -d classes src/udp/tchat/*.java`  
`java -cp classes udp.tchat.Tchat`  

# Q1
    => voir code de la classe udp.tchat.Tchat
# Q2
    => on pourrait enregistrer dans une table de hachage
    chaque nouvelle adresse ip qu'on reçoit en
    l'associant à une chaîne de caractères qui
    répresentera un nom d'une machine où un utilisateur