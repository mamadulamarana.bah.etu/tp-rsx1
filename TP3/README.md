# Rapport pour le TP3

## Exercice 1 : Première expérience
### Q1
    pour traiter une requête de connexion au serveur, il faut tout
    d'abord un socket pour le client pour le client qui va essayer de se
    connecter et aussi que le serveur accepte accepter la connexion.
    ensuite il faut repondre au client qui s'est connecté afin de lui
    notifier que la connexion a été etablie avec succès.

### Q2
    il faut s'assurer de traiter les exceptions de types IOException qui
    peuvent être déclenché quand on essaie d'accepter une connexion,
    d'envoyer un message au client et quand on essaie de fermé un socket
    client ou serveur.

### Q3
    pour tester le bon fonctionnement du programme, on peut utiliser le
    client telnet en se connectant au serveur via l'adresse ip et le port 
    spécifié.
    => dans notre cas :
`telnet a1xxpxx.fil.univ-lille.fr 2023`
![Q3](screen/telnetEx1.png)

### Q4
    on peut utiliser une structure de données telle qu'une liste où une
    Map (pour plus d'informations) pour stocker les connexions qui ont
    déjà été acceptée par le serveur.

![Q3](screen/screen1Ex1Q4.png)

## Exercice 2 : Serveur de Dialogue

### Q1
    on crée un thread après q'une connexion ait été accepté par le
    serveur. ensuite on attache ce thread au socket ainsi accepté.

### Q2
    les primitives getInputStream() et getOutputStream() nous permet de
    recevoir des chaines de caractères sur un socket en utilisant les
    classes InputStream et OutputStream et, aussi un BufferReader pour 
    stocker les cractères reçus.

### Q3
    en enregistrant toutes les connexions établies avec le serveur dans 
    une structure de données (une liste ici) defini en tant varible de
    classe (variable globale) qu'on pourra passer à chaque thread qui a
    son tour se mettra en attente de reception d'un message et ainsi le
    transmettre a tous les sockets presentes dans notre structure de 
    données (ceci est fait par la méthode share(message, connexions) dans
    la classe client dans notre cas).

### Q4
    en les stockants dans notre liste connexions (voir code)

### Q5
    lorsqu'un client telnet quitte la connexion de manière normale où
    intempestivement, on doit veuiller à supprimer le socket 
    correspondant dans notre liste connexions afin d'éviter de
    transmettre a un client indisponible, et aussi veuiller à bien fermer
    le socket client qui s'est deconnecté.

