package tcp;

import java.net.*;
import java.util.*;
import java.io.*;

public class ServerTcpEx2 {
    static List<Socket> connexions = new ArrayList<>();

    public void setConnexions(List<Socket> connexions) {
        ServerTcpEx2.connexions = connexions;
    } 
    public static void main(String[] args) {
        ServerSocket sose;
        Socket sock;

        try {
            sose = new ServerSocket(2023);
        } catch (IOException e) {
            System.out.println("Socket exception");
            return;
        }

        String msg = "Bienvenue sur mon serveur\n";
        byte[] buffer = msg.getBytes();

        while (true) {

            try {
                sock = sose.accept();
            } catch (IOException e) {
                System.out.println("Socket exception");
                return;
            }
            OutputStream out = null;
            try {
                out = sock.getOutputStream();
            } catch (IOException e) {
                System.out.println("error In or Out");
            }

            if (!ServerTcpEx2.connexions.contains(sock)) {
                try {
                    out.write(buffer);
                } catch (IOException e) {
                    System.out.println("erreur ecriture");
                }
                ServerTcpEx2.connexions.add(sock);
            }

            Thread th;
            th = new Thread(new Client(sock, ServerTcpEx2.connexions));
            th.start();

            try{
                th.join();
            }catch(InterruptedException e){
                System.out.println("le thread ne se termine pas correctement");
                return;
            }
            ServerTcpEx2.connexions.remove(sock);
            try {
                sock.close();
            } catch (IOException e) {
                System.out.println("erreur fermeture");
            }
            if (ServerTcpEx2.connexions.size() == 0){
            System.out.println("personne");
        }

        }
    }
}
