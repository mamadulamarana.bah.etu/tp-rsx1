package tcp;

import java.net.*;
import java.util.*;
import java.io.*;

public class ServerTcpEx1 {

    public static void main(String[] args) {
        ServerSocket sose;
        Socket sock;

        List<InetAddress> connexions = new ArrayList<>();

        try {
            sose = new ServerSocket(2023);
        } catch (IOException e) {
            System.out.println("Socket exception");
            return;
        }

        String msg = "Bienvenue sur mon serveur et au revoir\n";
        byte[] buffer = msg.getBytes();

        while (true) {

            try {
                sock = sose.accept();
            } catch (IOException e) {
                System.out.println("Socket exception");
                return;
            }
            InetAddress ip = sock.getInetAddress();
            if (!connexions.contains(ip)) {
                connexions.add(ip);
            }
            
            OutputStream out = null;
            try {
                out = sock.getOutputStream();
            } catch (IOException e) {
                System.out.println("error In or Out");
            }
            
            try {
                out.write(buffer);
            } catch (IOException e) {
                System.out.println("erreur ecriture");
            }

            try {
                sock.close();
            } catch (IOException e) {
                System.out.println("erreur fermeture");
                return;
            }
        }
    }
}