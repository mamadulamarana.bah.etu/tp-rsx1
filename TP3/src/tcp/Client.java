package tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

public class Client implements Runnable {

    protected Socket s;
    protected List<Socket> connexions;
    protected InputStream in;
    protected OutputStream out;

    public Client(Socket s, List<Socket> connexions) {
        this.s = s;
        this.connexions = connexions;
        this.in = null;
        this.out = null;
    }

    public void share(String message, List<Socket> c) {
        byte[] buffer = message.getBytes();
        for (Socket ad : c) {
            if (ad != this.s) {
                try {
                    this.out = ad.getOutputStream();
                    this.out.write(buffer);
                } catch (IOException e) {
                    System.out.println("erreur envoie");
                }
            }
        }
    }

    public void run() {

        BufferedReader bf;
        String message = null;

        while (true) {
            try {
                this.in = this.s.getInputStream();
            } catch (IOException e) {
                System.out.println("erreur lecture 1 message (InputStream)");
                return;
            }

            bf = new BufferedReader(new InputStreamReader(this.in));
            try {
                message = bf.readLine();
                if (message == null){
                    return;
                } 
                message += "\n";
            } catch (IOException e) {
                System.out.println("erreur lecture message");
            }
            this.share(message, this.connexions);
        }
    }
}
