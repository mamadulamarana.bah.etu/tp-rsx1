import java.net.*;
import java.io.*;

public class ClientTcp {

    public static void main(String[] args) {
        Socket s = null;
        try {
            s = new Socket("a04p"+args[0]+".fil.univ-lille.fr", 2023);
        } catch (IOException e) {
            System.out.println("Socket exception");
            return;
        }

        InputStream iS = null;
        OutputStream oS = null;
        try {
            iS = s.getInputStream();
            oS = s.getOutputStream();
        } catch (IOException e) {
            System.out.println("error iS or Os");
        }
        
        int v;
        try {
            v = iS.read();
            oS.write(v);
        } catch (IOException e) {
            System.out.println("error v");
        }

        BufferedReader bF;
        bF = new BufferedReader(new InputStreamReader(iS));
        try {
            String str = bF.readLine();
        } catch (IOException e) {
            System.out.println("error readline() ");
        }

        


        try {
            s.close();
        } catch (IOException e) {
            System.out.println("erreur a la fermeture du socket");;
        }
    }
}