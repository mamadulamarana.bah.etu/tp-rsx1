# Rapport pour le TP1
# UDP
## Q1 a Q9
### Q6
    pour qu'on puisse envoyer un message a notre il faut q'uil nous fournisse
    son adresse IP et le numéro de port via lequel transmettre.
## Q8
    Il est préferable de laisser le systeme choisir
    automatiquement le port sur la machine qui envoie
    le message initial, et de choisir manuellement le
    port de destination.
![Q1 a Q9](screen/screen1.png)

## Q10 a Q

![Q10 a Q](screen/screen2.png)
![socket](screen/screen3.png)
![wireshark](screen/screen4.png)
![Q13](screen/screenq13.png)
![15-b](screen/graphique1.jpg)
## Q15.C
    2 segments UDP a été transmis, l'un pour l'envoi
    du message du Port soure (59861) et l'autre pour
    l'envoi de la reponse du Port dest (57548).

## Q15.D
    l'efficacité du protocole est de 0.30

# TCP
![Q10 a Q](screen/TCP.png)

## Q5
    la connexion ne fonctionne pas entre les deux
    sockets car  S2 n'est pas en mode ecoute d'une 
    autre source.
![Q10 a Q](screen/screenTcP2.png)
    
## Q6
    le socket serveur est 3 et le socket client est 4

## Q7
    le flag SYN (Synchronisation) a été activé pour
    permettre la synchronisation et etablir la
    connexion.

## Q8
![Q10](screen/screenTcP3.png)

## Q11
![Q11](screen/q11.png)

### Q11.a
    si le flag PSH (push) n'avait pas été activé par
    l'emetteur, le message envoyé n'aurait pas été
    poussé et serait resté en attente, et donc le
    message n'aurait pas été réçu.

### Q11.b
    le numero de séquence du segment envoyé
    correspond a un numéro unique attribué au segment
    pour garantir la reception de données dans
    l'ordre dans lequel ils ont été envoyées.
    ce numero de sequence defini l'octet du message
    réçu.

### Q11.c
    le numero d'acquitement du segment réçu
    correspond au numero de séquence (seq) du
    prochain octet à transmettre. cela definit le
    prochain octet à transmettre et garantit le bon
    ordre de reception du message.

### Q11.d
    ce résultat correspond à la capacité de reception
    du recepteur (port 4000). c'est à dire au nombre
    d'octets qu'on peut lui transmettre sans 
    provoquer de congestion du réseau.

## Q12
    le champ Recv-Q (queue de reception) contient le
    nombre d'octets transmis par le port(3000) vers
    le port(4000) qui n'ont pas encore été lue.
![Q12](screen/q12.png)

## Q13
    <id_socket> représente l'identifiant renvoyé par
    la commande socket.
![Q13](screen/q13.png)

## Q14
    le champ Recv-Q ne contient désormais aucun 
    octets en attente de lecture parce qu'on a utilisé la commande : read 3 20.
![Q14](screen/q14.png)

## Q15 
    le message n'est pas envoyé car le socket de
    reception n'est plus connecté au socket source
![Q15](screen/q15.png)
![Q15](screen/wireshark.png)

## Q16
![Q16](screen/q16.png)

## Q17
    la commande shutdown met fin a la connexion entre 
    un socket serveur et un socket client dans un
    le sens précisé.
![Q17](screen/wireshark.png)
![Q17](screen/q17.png)

## Q18

### Q18.a
![Q18](screen/graphique2.png)
    il y'a eu 10 segments qui ont été transmis avecl'envoi
    d'acquitements àchaque bonne reception d'un segment TCP.

### Q18.b
    il y' environ 10 segments TCP qui ont été 
    transmis tandis que seulement 2 ont été transmis
    avec UDP.

### Q18.c
    l'efficacité est de 0.25 tandis qu'avec le
    protocole UDP nous avions obtenu 0.30. le protocole
    TCP paraît être plus éfficace que le protocole UDP


# Retransmissions et contrôle de flux

## Q1
![Q1](screen/partie3-q1.png)

## Q6
    on constate qu'on a envoyés 1960705 octets du 
    port 3000 vers le port 4000 mais qu'il a
    seulement réçu 61916 octets.
![Q6](screen/partie3-q6.png)

## Q7
    nous constatons l'envoie de plusieurs segments
    TCP vers le port 4000, et l'echange a été stoppé
    quand on a appuyé sur Ctrl-C.
![Q7](screen/partie3-q8.png)
![Q7](screen/partie3-q8-bis.png)

## Q8
![Q8](screen/partie3-q8.png)
![Q8](screen/partie3-q8-bis.png)
### Q8.a

### Q8.b
    l'emetteur S1 garde en vie (Keep-Alive) la 
    connexion, c'est à dire qu'elle maintient la 
    connexion active entre les sockets en attendant
    de recevoir à nouveau des acquittements.

### Q8.c
    la connexion se termine automatiquement dès que
    toutes les données du buffer de reception ont
    ont été exploitées parce qu'il ne reçoit plus
    d'acquittement.

### Q8.d
    oui le destinataire transmet un accusé de 
    reception à chaque segment réçu, cela répresente
    l'acquittement et permet à l'expéditeur d'emettre
    à nouveau tout en étant sûre que les données 
    envoyées ont été bien réçu à chaque fois.

### Q8.c
    la taille de la fenêtre de réception (Win)
    TCP est dynamique et peut varier en fonction de 
    plusieurs facteurs, y compris les conditions du 
    réseau et la capacité du récepteur à traiter les 
    données.
    la fenêtre de réception varie ne fonction de la
    capacité du recepteur à recvoir des données, elle
    peut être augmenté pour permettre une un 
    transfert de données plus rapide oû réduite pour
    pour éviter de surcharger un réseau déjà
    congestionné.

